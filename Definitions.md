TODO

- issue creator
- moderator
- participant
- group council member
- community council member

--- OLD ---

# Bodies

## Community

1. The community is the entirety of its members.
2. A community member is anyone who identifies themselves as such, unless they're explicitly excluded (e.g. for severe violation of the code of conduct). They can demonstrate their affiliation, for instance by activity or accounts.
3. There's no obligatory demand from community members, unless otherwise stated in contracts (e.g. work contracts). This determines a do-ocracy at the foundation of the community – anyone is free to act or leave – the dynamic and survival of the community is determined accordingly.
3. All community members are equally amenable to all community regulations (e.g. the code of conduct or this statute) and have otherwise the full freedom of opinion, expression (incl. speech) and action – at least as far as the community is concerned.
4. A community's life force is it's members who take a) active part or roles in it and b) ownership of it. Most of the roles come with a certain responsibility and may require specific knowledge and skills. In order to ensure the stability of the community's performance and sustainability (e.g. preserving its values), selected roles are limited to members with attested qualification. Qualifications are represented by licenses. As defined in `Bod §1 sec. 3`, all community members have the right to acquire any license.

## Licenses

1. Licenses, and the processes to acquire them, are defined in open standards. These standards can be adjusted or retracted in accordance with the standardization process.
2. Licenses can be permanent or temporary. The license of a member can be invalidated by a circle or a mechanism defined in the corresponding standard.

<!--- definition of a qualified circle for standardization processs; standardization by "unqualified" circles is possible but will probably require more iterations -->

<!--- start list of licenses  -->

## Circle

### Basics

1. A circle is a community body to take decisions on behalf of the community or a defined part of it. Doing so it must represent the needs and wishes of the all affected members, which ultimately also includes the well-being of the community as a whole. Consequently, the assembly may be continuously adjusted in order to fulfill the purpose of the circle.
2. A circle can be created by an already existent circle or a community member with a democrat license.
3. A circle may involve externs, appointed by the members of the circle.
4. Decisions of a circle can be overthrown by another circle. A revaluation however requires full consent of the previous circle – or, if full consent is not achieved, an arbitral decision. The same applies for competing circles that act in parallel.
	- The format of the arbitral decision is decided by the responsible circle (most likely an infrastructure circle).
	- If no responsible circle was defined, the decision falls to the council, which may redirect it to a selected circle.

<!--- make a list of arbitral decision formats (e.g. vote within the circle, community vote, creation of a qualified issue circle) -->

### Issue Circle

- decided by consensus how they want to make decisions. goal: consensus, otherwise consent or ultimately: vote

qualified issue circle: issue circle designed according to an open standard to ensure certain qualities of it (e.g. certain expertise or representativeness)

### Infrastructure Circle

- Infrastructure circles are formed
	- for an unlimited period
	- subject to a defined scope (which also also defines their mandate)

have to be qualified circles

- e.g. Council, IT
- manages arbitral decisions

## Plenum

is a circle

if a consent fails, the issue automatically escalates to the council which then responsible for it (e.g. assembling a new Plenum, calling in further experts or taking a vote among community members)

## Council

acts like a permanent circle with special powers