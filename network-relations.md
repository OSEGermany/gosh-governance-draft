# Network Relations

## Terms & Definitions

### Community-Owned Organization

- a legal body that is by >50 in the possession of community members
    - the number of possessing community members should correlate with the significance of that organization
- legal body is seen as part of the community

### Community Partner Organization

- a legal body that:
    - aligns with the values of the community
    - has expressed a binding commitment to collaborate with the community within a defined scope
- collaboration can be indefinite or definite

### Community Supplier Organization

- a legal body that is supplying the community through a partnering or owned legal body
- not seen as part of the community

## Example Cases

### Decentralized Production and Services under Centralized Trademark

- trademark is owned by a [Community-Owned Organization](#community-owned-organization)
- trademark can be licensed to certified partners or suppliers
- certification follows an open standard
- further conditions may apply (e.g. having the certification renewed every couple of years)

### Community Assets

- Anything of significant value has to be possessed by a [Community-Owned](#community-owned-organization) or [Partner Organization](#community-partner-organization) – or controlled by either of them e.g. via a contract.
    - Example: IT infrastructure hosted on the free market by a [Community Supplier Organization](#community-supplier-organization) – and administered by the community itself (no legal body required for administration)

