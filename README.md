# GOSH Governance Draft

## Licensing

All content in this repository is licensed under Creative Commons CC0 1.0 Universal, please see [LICENSE](LICENSE.md) for details, **except** for redistributed material, as referenced in [ATTRIBUTION](ATTRIBUTION.md).
