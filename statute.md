---
title: Community Statue
...

# Rationale

Open source communities are inherently borderless and hence do not adhere to the limitations of national states.
Yet, they need a resilient interface to the local jurisdiction in order to drive their efforts, e.g. publicly funded projects.
Since a community is no legal body per se[^community-legal-body] they can found, or partner with, organizations on the ground.
These organizations can exist in many different states and partner with a variety of other communities themselves – some of these organizations may even be mere legal multi-tools.

[^community-legal-body]: lawyers may disagree here, but this is not my point

Consequently, a community may enter a partnership with a legal body to do things it cannot do by itself, for instance to:

- have and administer property (e.g. money),
- conclude claimable contracts with legal bodies.

This draft aims to provide a copyable statute to enable a resilient and efficient community-driven decision-making, capable of negotiating resil#ient partnership agreements across the globe.

# Decision-Making

## Intro

The aim of this process is, for a given issue, to achieve a decision that all involved parties can directly consent with – or at least trust, as they trust in the process.

Everyone affected by an issue must a) have a fair chance to take part in the decision-making and b) (in case that they didn't participate themselves) be able to understand all decisions taken in the process.

If the resulting group of people is small enough to take a decision by consent directly in a meeting, as well as in other situations that come with an obvious/trivial solution, the process below can be skipped of course.

## General Scheme

roles:

- issue creator
- moderator
- participant
- group council member
- community council member

format:

- issues are visible and accessible without any restrictions other than member access rights
- the process has been drafted with the format of GitLab issues in mind

assumptions:

- all roles exist
- all people involved have access to all information and the process
- all people involved act with goodwill; constructive contributions only
- issue creator wants a timely solution for their issue
- plenum represents the perspective (this part of) the community
- documentation provides enough information for anyone interested to understand the taken decisions

1. **issue is created**\
	any community member can open an issue:
	- describing the problem and the schedule (=defining the time pressure behind it, which might differ among solution options)
	- naming its moderator (can be the issue creator; moderator can change over time)
	- optionally:
		- mapping out core background information and obvious solution options + consequences
		- suggesting groups/roles/specific people that should be involved in the decision-making
2. **call for participation (CFP) for mapping out solutions and consequences**
	- the moderator summarizes and communicates the issue to everyone who's affected by the decision (directly or indirectly) or part of the decision-making chain (e.g. group councils) – e.g. in a community chat channel
	- anyone can join the gathering of information and reflection on possible solutions and resulting consequences; the moderator makes sure that documentation in the actual issue remains readable and structured (e.g. having a dedicated chat channel just for discussions and only copying significant points into the issue description)
	- if this process takes a long time or unforeseen twists it's recommended to rerun the CFP as new people might want to join the process then
	- anyone is also free to invite people directly
	- once the discussion reach a stable point or the time schedule of the issue has arrived, the moderator moves over to the next step:
3. 	**plenum for consent (PFC)**\
	a small group people (3…9) meets up and takes a decision based on the content in the issue
	- the plenum aims to represent a) everyone affected and b) everyone involved in implementing the solutions; hence this is the goal of mechanisms to create a subset of participants for the plenum (e.g. a weighted lottery)

### Examples

#### A) Council Decides

![Flowchart for Council-only decisions](graphics/1a.svg)

**EXAMPLE:**

- administrative issues

#### B) Council Calls a Plenum Decision

![Flowchart for Plenum-supported Council decisions](graphics/1b.svg)

**EXAMPLE:**

- issues requiring special expertise (e.g. regarding IT infrastructure)
- issues with long-term consequences for the community

#### C) Plenum Decides, Council Approves

![Flowchart for Council-approved Plenum decisions](graphics/1c.svg)

**EXAMPLE:**

- plenum decisions with impact on the community administration
- cooperations with external organizations (initiated by community members) that require approval from the council

#### D) Plenum Decides 

![Flowchart for Plenum-only decisions](graphics/1d.svg)

**EXAMPLE:**

- internal project decision that needs a complex expert advice

## Foreign Affairs

### A) Partnership

A trusted party, e.g. a local legal body or another community, can become a partner of the community.
Both entities keep their independence, but choose to collaborate in certain points.

The community discusses and determines the conditions of a collaboration with a partner organization.
This happens in an Issue Circle that involves members of the partner organization.
The result is here called "Community Agreement (CA)", but you may choose to give it a sexier name.
Once the agreement is concluded, both, the community and the partner organization, transfer it to their decision-making bodies so that it can be officially endorsed and take effect.
Once done, each party sends a written confirmation.

The content of the final agreement might be made publicly available as a good practice for transparent governance, but more restrictive measures may be agreed e.g. to protect sensitive data.

The commitment to the agreement is solely based on mutual trust – no consequences arise from breaking it, other than respective sanctions from the involved parties.
It's a highly recommendable best practice to define an exit strategy for _any_ agreement.
However, depending on the risk both parties take thereby, measures might be recommended to secure the commitment – e.g. reciprocal influence by dedicated roles in the other party.

<!--- update graphic -->

![Flowchart for a partnership](graphics/2a.svg)

**EXAMPLE:**

- agreement for the hosting of publicly funded projects
- agreement for the provision of infrastructure

### B) External Contracts

If a contract is to be concluded with an external organization that cannot be seen as a partner (e.g. an unrelated actor on the market), the community can use a Partnership to establish this connection.
The contract is concluded between partner and external organization, the conditions of this setup defined in the Community Agreement (CA).

However, the CA should acknowledge that the community is not a legally liable partner in this setup, so the partner should be clear about its conditions.

![Flowchart for an external contract via a partner organization](graphics/2b.svg)

**EXAMPLE:**

- any claimable contract with standard market actors
- employment contracts
- acquisition processes

# Mandates

<!--- define the creation and management of roles with specific scope, responsibility and mandate, e.g. finance lead, circle moderator, PR person -->
